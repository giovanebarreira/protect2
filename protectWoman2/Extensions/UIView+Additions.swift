//
//  UIView+Additions.swift
//  ProtectWoman
//
//  Created by Matheus Santos on 15/10/18.
//  Copyright © 2018 Matheus Santos. All rights reserved.
//
import UIKit

extension UIView {
    func smoothRoundCorners(to radius: CGFloat) {
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(
            roundedRect: bounds,
            cornerRadius: radius
            ).cgPath
        layer.mask = maskLayer
    }
}
