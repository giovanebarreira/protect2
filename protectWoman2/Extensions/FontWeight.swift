//
//  FontWeight.swift
//  protectWoman2
//
//  Created by giovane barreira on 29/10/18.
//  Copyright © 2018 giovane barreira. All rights reserved.
//

import Foundation

enum FontWeight: String {
    case icomoon = "icomoon"
    case nowBold = "Now-Bold"
    case nowRegular = "Now-Regular"
}
