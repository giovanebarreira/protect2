//
//  Colors.swift
//  protectWoman2
//
//  Created by giovane barreira on 29/10/18.
//  Copyright © 2018 giovane barreira. All rights reserved.
//

import UIKit

extension UIColor {
    static let purpleTMJ = #colorLiteral(red: 0.2196078431, green: 0.07843137255, blue: 0.3529411765, alpha: 1)
    static let pinkTMJ = #colorLiteral(red: 0.9538021684, green: 0.4416917562, blue: 0.5280824304, alpha: 1)
}
