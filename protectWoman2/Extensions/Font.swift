//
//  Font.swift
//  protectWoman2
//
//  Created by giovane barreira on 29/10/18.
//  Copyright © 2018 giovane barreira. All rights reserved.
//

import UIKit

extension UIFont {
    class func fontIcomoon(ofSize fontiSize: CGFloat) -> UIFont {
        return customFont(ofSize: fontiSize, withWeight: .icomoon)
    }
    
    class func fontNowBold(ofSize fontiSize: CGFloat) -> UIFont {
        return customFont(ofSize: fontiSize, withWeight: .nowBold)
    }
    
    class func fontNowReguar(ofSize fontiSize: CGFloat) -> UIFont {
        return customFont(ofSize: fontiSize, withWeight: .nowRegular)
    }
    
    class func customFont(ofSize fontSize: CGFloat, withWeight fontWeight: FontWeight) -> UIFont {
        return UIFont.init(name: fontWeight.rawValue, size: fontSize) ?? UIFont.boldSystemFont(ofSize: fontSize)
    }
}

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
