//
//  PhotoPickerViewController.swift
//  protectWoman2
//
//  Created by Matheus Santos on 08/11/18.
//  Copyright © 2018 giovane barreira. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class PhotoPickerViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var btnContinuar: UIButton!
    
    var imagePickers = UIImagePickerController()
    
    private var verificationUsers: DatabaseReference?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnContinuar.isEnabled = false
        
        addImagePickerToContainerView()
        
        let currentUserUID = Auth.auth().currentUser?.uid
        
        verificationUsers = Database.database().reference(withPath: "Users/\(currentUserUID ?? "dead")")
        
    }
    

    @IBAction func takePictureBtn(_ sender: Any) {
       imagePickers.takePicture()
        btnContinuar.isEnabled = true
        
    }
    
    @IBAction func continueBtn(_ sender: Any) {
        if imageView.image != nil {
            let imageData: Data? = imageView.image!.jpegData(compressionQuality: 0.0)
            let imageStr = imageData?.base64EncodedString(options: .lineLength64Characters) ?? ""
            let dataDict: [String: Any] = ["userImage": imageStr, "isVerified": false, "displayName": Auth.auth().currentUser?.displayName as Any, "email": Auth.auth().currentUser?.email as Any]
            
            verificationUsers?.setValue(dataDict)
        }
        self.performSegue(withIdentifier: "toLoginVerifier", sender: self )
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if let destination = segue.destination as? VerificationViewController {
            if let picture = imageView.image {
                destination.userImage = picture
            }
        }
    }
    
    func addImagePickerToContainerView() {
        
        imagePickers = UIImagePickerController()
        if UIImagePickerController.isCameraDeviceAvailable( UIImagePickerController.CameraDevice.front) {
            imagePickers.delegate = self
            imagePickers.sourceType = UIImagePickerController.SourceType.camera
            imagePickers.cameraDevice = .front
            
            //add as a childviewcontroller
            addChild(imagePickers)
            
            // Add the child's View as a subview
            self.imageView.addSubview((imagePickers.view)!)
            imagePickers.view.frame = imageView.bounds
            imagePickers.allowsEditing = false
            imagePickers.showsCameraControls = false
            imagePickers.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
        }
    }
}

extension PhotoPickerViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imagePickers.view.removeFromSuperview()
            imageView.contentMode = .scaleAspectFit
            imageView.image = selectedImage
        }
        //dismiss(animated: true, completion: nil)
    }
    
    
    
    
}
