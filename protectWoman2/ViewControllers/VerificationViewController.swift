//
//  VerificationViewController.swift
//  protectWoman2
//
//  Created by Matheus Santos on 08/11/18.
//  Copyright © 2018 giovane barreira. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import FirebaseMessaging



class VerificationViewController: UIViewController {

    var userImage: UIImage!
    var currentUserUID: String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentUserUID = Auth.auth().currentUser?.uid

        
        
        let verifiedUsers = Database.database().reference(withPath: "Users/\(currentUserUID ?? "dead")/isVerified")
        
        verifiedUsers.observe(.value, with: {(snapshot) in
            guard let isVerified = snapshot.value as? Bool else {return}
            if isVerified {
                self.dismiss(animated: true, completion: nil)
            }
        })
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        navControllerStyle()
        
        
    }
    @IBAction func logout(_ sender: Any) {
        try! Auth.auth().signOut()
        self.performSegue(withIdentifier: "toLogin", sender: self)
    }
    
    func navControllerStyle() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.2171504796, green: 0.07647859305, blue: 0.3546665907, alpha: 1), NSAttributedString.Key.font: UIFont(name: "Now-Bold", size: 20)!]
    }
}
