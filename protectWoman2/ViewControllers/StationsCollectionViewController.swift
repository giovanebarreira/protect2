//
//  StationsCollectionViewController.swift
//  protectWoman2
//
//  Created by Bruna Gagliardi on 24/10/18.
//  Copyright © 2018 giovane barreira. All rights reserved.
//

import UIKit
import Firebase
import MessageKit


class StationsCollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    // MARK: Instancias
    private var chats: [String] = []
    private var chatsRef: DatabaseReference?
    private var usersRef: DatabaseReference?
    
    
    private var clickedChat: String!
    private var chatPath: String!
    private var users = [String : [String]]()

    var startAndEndTimeDic = [String: (startTime: Date, endTime: Date)]()
    
    var lineAndStation: (line: String, station: String)!
    
    @IBOutlet weak var myCollectionView: UICollectionView!
    
    
    // MARK: LifeCicle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        chatPath = "Stations/Metro/\(lineAndStation.line)/Stations/\(lineAndStation.station)/chats"
        print(chatPath)
        
        chatsRef = Database.database().reference(withPath: chatPath)
        print(lineAndStation)
     
        guard let chatsRef = chatsRef else {return}
        
        
        chatsRef.observeSingleEvent(of: .value, with: { (snapshot) in
            for child in snapshot.children {
                if let rooms = child as? DataSnapshot {
                    self.chats.append(rooms.key)
                    
                    let snap2 = rooms.childSnapshot(forPath: "users-online")
                    var arr = [String]()
                    for child2 in snap2.children {
                        if let userSnap = child2 as? DataSnapshot {
                            if let user = userSnap.value as? String{
                                arr.append(user)
                            }
                        }
                    }
                    self.users[rooms.key] = arr
                    
                    
                    //getting the useful stuff
                    let snap3 = rooms.childSnapshot(forPath: "useful_stuff")
                    
                    guard let value = snap3.value as? [String: AnyObject] else {fatalError("could not get value")}
                    guard let closingTime = value["endTime"] as? String else {fatalError("could not get closing time")}
                    guard let openingTime = value["startTime"] as? String else {fatalError("could not get opening time")}
                    let dateFormatterGet = DateFormatter()
                    dateFormatterGet.dateFormat = "HH:mm"
                    self.startAndEndTimeDic[rooms.key] = (startTime: dateFormatterGet.date(from: openingTime)!, endTime: dateFormatterGet.date(from: closingTime)!)

                }
            }

            self.myCollectionView.reloadData()
            
        })
        
       
       

    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    
    
    // MARK: UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return chats.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myCell", for: indexPath) as! StationCollectionViewCell

        let chatName = self.chats[indexPath.row]
        
        
        let startAndEnd = startAndEndTimeDic[chatName]!
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "HH:mm"
        
        cell.stationLabel.text = "\(dateFormatterGet.string(from: startAndEnd.startTime)) -> \(dateFormatterGet.string(from: startAndEnd.endTime))"
        
        
        
        cell.numberUsersLabel.text = "\(users[chatName]!.count)/20"
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        clickedChat = chats[indexPath.row]
        
        performSegue(withIdentifier: "toChat", sender: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        cell.contentView.layer.cornerRadius = 10.0
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        cell.contentView.layer.masksToBounds = true;
        
        cell.layer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cell.layer.cornerRadius = 10
        cell.layer.shadowColor = #colorLiteral(red: 0.9098039216, green: 0.5431006551, blue: 0.7233027816, alpha: 1)
        cell.layer.shadowOffset = CGSize(width:0,height: 4)
        cell.layer.shadowRadius = 5
        cell.layer.shadowOpacity = 0.75
        cell.layer.masksToBounds = false;
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
    }
    
    //MARK: Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? ChatViewController {
            destinationVC.activeChat = chatPath + "/\(clickedChat!)"
        }
    }
    
}
