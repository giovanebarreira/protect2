//
//  LoginViewController.swift
//  ProtectWoman
//
//  Created by Matheus Santos on 10/10/18.
//  Copyright © 2018 Matheus Santos. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import Messages

class LoginViewController: UIViewController, GIDSignInUIDelegate {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = true
        
        //Define o delegate do botao de login da google
        GIDSignIn.sharedInstance().uiDelegate = self
        //Cria o botao de login do facebook

        
        //self.view.addSubview(loginButton)
        
    
        
       
        
        
        
        //fica de olho se o usuario logou
        Auth.auth().addStateDidChangeListener { (auth, user) in
            if user != nil {
                //self.dismiss(animated: true, completion: nil)
                self.performSegue(withIdentifier: "toPhotoPicker", sender: self)
                //Somente para ver se o login foi feito com sucesso
               
                
               
            }
        }
        
    }
    
 
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.isHidden = true
    }

    
    
}

