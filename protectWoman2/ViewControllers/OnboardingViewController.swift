//
//  OnboardingViewController.swift
//  protectWoman2
//
//  Created by giovane barreira on 23/10/18.
//  Copyright © 2018 giovane barreira. All rights reserved.
//

import UIKit
import paper_onboarding


class OnboardingViewController: UIViewController{
    
    //Outlets
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var onboardingObj: OnboardingViewClass!
    
    //Variables
    var userData = UserDefaults.standard
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        onboardingObj.dataSource = self
        onboardingObj.delegate = self
        
    }
    
    //Actions
    @IBAction func doneButtonAction(_ sender: Any) {
        userData.set(true, forKey: "completed")
        userData.synchronize()
    }
    
}

extension OnboardingViewController: PaperOnboardingDataSource, PaperOnboardingDelegate{
    func onboardingItemsCount() -> Int {
        return 3
    }
    
    func onboardingItem(at index: Int) -> OnboardingItemInfo {
        let bgOne = #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 0)
        let textColor = UIColor.purpleTMJ
        let titleColor = UIColor.pinkTMJ
        
        let titleFont = UIFont.fontNowBold(ofSize: 26)
        let textFont = UIFont.fontNowReguar(ofSize: 16)
        
        
        //ver negrito
//        let formattedString = NSMutableAttributedString()
//        formattedString
//            .normal(" Somos um app ")
//            .bold("que conecta as mulheres nos metrôs e trens São Paulo,")
//            .normal("para prevenir possíveis assédios!")
//
//
//
//        let lbl = UILabel()
//        lbl.attributedText = formattedString
        
        
     
        
        
        return[OnboardingItemInfo(informationImage: #imageLiteral(resourceName: "imgPerfil"), title: "Quem Somos?", description: "Somos um app que conecta as mulheres nos metrôs e trens São Paulo, para prevenir possíveis assédios!", pageIcon: #imageLiteral(resourceName: "imgPerfil"), color: bgOne, titleColor: titleColor, descriptionColor: textColor, titleFont: titleFont, descriptionFont: textFont),
               
          OnboardingItemInfo(informationImage: #imageLiteral(resourceName: "imgPerfil"), title: "Mas como?", description: "Criamos chats exclusivos para mulheres, conectando-as para andarem juntas nos metrôs e trens", pageIcon: #imageLiteral(resourceName: "imgPerfil"), color: bgOne, titleColor: titleColor, descriptionColor: textColor, titleFont: titleFont, descriptionFont: textFont),
       
         OnboardingItemInfo(informationImage: #imageLiteral(resourceName: "imgPerfil"), title: "Notificação do Chat!", description: "E também precisamos da sua permissão de notificação para avisar as novas mensagens no chat do app!", pageIcon: #imageLiteral(resourceName: "imgPerfil"), color: bgOne, titleColor: titleColor, descriptionColor: textColor, titleFont: titleFont, descriptionFont: textFont)][index]
    }
    
    func onboardingDidTransitonToIndex(_ index: Int) {
        if index == 2 {
            doneBtn.isHidden = false
            doneBtn.layer.zPosition = 10
        }
    }
    
    func onboardingWillTransitonToIndex(_ index: Int) {
        if index != 2{
            if doneBtn.isHidden == false{
                doneBtn.isHidden = true
            }
        }
    }
    
    func onboardingConfigurationItem(_ item: OnboardingContentViewItem, index _: Int) {
        
    }
    
}
