//
//  StationCollectionViewController.swift
//  protectWoman2
//
//  Created by Bruna Gagliardi on 18/10/18.
//  Copyright © 2018 giovane barreira. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class LinesCollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    // MARK: Instancias
    
    @IBOutlet weak var myCollectionView: UICollectionView!

    private var printTucuruvi = false
    private var lines:[Lines] = []
    private var directions:[String]? = []
    private var index:Int?
    private var stationRef: DatabaseReference?
    private var lineAndStation: (line: String, station: String)!
    
    // MARK: LifeCicle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Auth.auth().currentUser == nil {
            self.performSegue(withIdentifier: "toLogin", sender: self)
        }
        
        let verifiedUsers = Database.database().reference(withPath: "Users/\(Auth.auth().currentUser?.uid ?? "")/isVerified")
        
        verifiedUsers.observe(.value, with: {(snapshot) in
            guard let isVerified = snapshot.value as? Bool else {
                self.performSegue(withIdentifier: "toLogin", sender: self)
                return
            }
            if !isVerified {
                self.performSegue(withIdentifier: "toVerification", sender: nil)
            }
        })
        
        let linesRef = Database.database().reference(withPath: "Stations/Metro")
        linesRef.observeSingleEvent(of: .value, with: { snapshot in
            for child in snapshot.children {
                if let snap = child as? DataSnapshot {
                    let line = Lines(snapshot: snap)
                    if line.name == "01Linha-1 - Azul" {
                        self.lines.append(line)
                    }
                    self.lines.append(line)
                }
            }
        
            for station in self.lines {
                print(station.name)
                for stationName in station.stationArray {
                    NSLog("LinesColletion: \(String(describing: stationName.name))")
                }
                
            }
            self.myCollectionView.reloadData()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
    navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.2171504796, green: 0.07647859305, blue: 0.3546665907, alpha: 1), NSAttributedString.Key.font: UIFont(name: "Now-Bold", size: 20)]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destination = segue.destination as? StationsCollectionViewController {
            destination.lineAndStation = lineAndStation
            
        }
    }
    
    @IBAction func logoutBtn(_ sender: Any) {
        try! Auth.auth().signOut()
        self.performSegue(withIdentifier: "toLogin", sender: self)
    }
    
    // MARK: UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return lines.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myCell", for: indexPath) as! LinesCollectionViewCell
        
        if lines[indexPath.row].name == "01Linha-1 - Azul" {
            cell.simbolLabel.text = "c q"
            cell.simbolLabel.textColor = #colorLiteral(red: 0.0003971238039, green: 0.3220826387, blue: 0.6128225923, alpha: 1)
            cell.linesLabel.textColor = #colorLiteral(red: 0.0003971238039, green: 0.3220826387, blue: 0.6128225923, alpha: 1)
            cell.linesLabel.text = "Azul"
        } else if lines[indexPath.row].name == "11Linha-11- Coral" {
            cell.simbolLabel.text = "z a"
            cell.simbolLabel.textColor = #colorLiteral(red: 0.9215686275, green: 0.3098039216, blue: 0.1411764706, alpha: 1)
            cell.linesLabel.textColor = #colorLiteral(red: 0.9233288169, green: 0.309907347, blue: 0.1411165595, alpha: 1)
            cell.linesLabel.text = "Coral"
        } else if lines[indexPath.row].name == "04Linha- 4 - Amarela" {
            cell.simbolLabel.text = "z r"
            cell.simbolLabel.textColor = #colorLiteral(red: 0.9886531234, green: 0.8412384391, blue: 0, alpha: 1)
            cell.linesLabel.textColor = #colorLiteral(red: 0.9886531234, green: 0.8412384391, blue: 0, alpha: 1)
            cell.linesLabel.text = "Amarela"
        } else if lines[indexPath.row].name == "07Linha- 7 - Rubi" {
            cell.simbolLabel.text = "z u"
            cell.simbolLabel.textColor = #colorLiteral(red: 0.6303697824, green: 0.0911366567, blue: 0.4014062285, alpha: 1)
            cell.linesLabel.textColor = #colorLiteral(red: 0.6303697824, green: 0.0911366567, blue: 0.4014062285, alpha: 1)
            cell.linesLabel.text = "Rubi"
        } else if lines[indexPath.row].name == "13Linha 13 - Jade" {
            cell.simbolLabel.text = "z d"
            cell.simbolLabel.textColor = #colorLiteral(red: 0, green: 0.5007320046, blue: 0.3864619732, alpha: 1)
            cell.linesLabel.textColor = #colorLiteral(red: 0, green: 0.5007320046, blue: 0.3864619732, alpha: 1)
            cell.linesLabel.text = "Jade"
        }
        
        if lines[indexPath.row].firstStation == "Tucuruvi" {
            
            if printTucuruvi == false {
                cell.stationLabel.text = "Sentido " + lines[indexPath.row].lastStation
                printTucuruvi = true
            } else {
                cell.stationLabel.text = "Sentido " + lines[indexPath.row].firstStation
                cell.simbolLabel.text = "x q"
            }
        } else if  lines[indexPath.row].firstStation == "São Paulo - Morumbi" {
            
            cell.stationLabel.text = "Sentido " + "SP - Morumbi"
            
        } else if lines[indexPath.row].firstStation == "Aeroporto - Guarulhos" {
            
            cell.stationLabel.text = "Sentido " + "Aéroporto GRU"
            
        } else {
            cell.stationLabel.text = "Sentido " + lines[indexPath.row].firstStation
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as? LinesCollectionViewCell
        
        var stationTreated = ""
        
        if cell?.stationLabel.text == "Sentido Tucuruvi" {
             stationTreated = lines[indexPath.row].lastStation.lowercased()
        } else {
            stationTreated = lines[indexPath.row].firstStation.lowercased()
        }
        stationTreated = stationTreated.replacingOccurrences(of: " ", with: "")
        
        stationTreated = "estacao-" + stationTreated
        
        let lineTreated = lines[indexPath.row].name.replacingOccurrences(of: " ", with: "")
        
        lineAndStation = (line: lineTreated, station: stationTreated)
        
        performSegue(withIdentifier: "StationView", sender: self)

        return print(lines[indexPath.row].name)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        cell.contentView.layer.cornerRadius = 10.0
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        cell.contentView.layer.masksToBounds = true
     
        cell.layer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cell.layer.cornerRadius = 10
        cell.layer.shadowColor = #colorLiteral(red: 1, green: 0.7510404587, blue: 0.7903534174, alpha: 1)
        cell.layer.shadowOffset = CGSize(width:0,height: 4)
        cell.layer.shadowRadius = 5
        cell.layer.shadowOpacity = 0.75
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
    }
}
