
import UIKit
import MessageKit
import MessageInputBar
import FirebaseDatabase
import FirebaseAuth
import FirebaseMessaging
import AudioToolbox


/// A base class for the example controllers
class ChatViewController: MessagesViewController, MessagesDataSource {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    //Array com a lista de mensagens
    var messageList: [Message] = []
    
    //Formatacao de Date
    let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()
    
    //Firebase
    var ref: DatabaseReference?
    var usersRef: DatabaseReference?
    
    //Chat
    var activeChat: String!
    private var activeChatFixed: String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Define os delegates das mensagens
        configureMessageCollectionView()
        //Define os delegates da barra de input
        configureMessageInputBar()
       
        //treates active chat for
        var chatArr = activeChat.components(separatedBy: "/")
        var lineName = chatArr[2]
        lineName.removeFirst(2)
        var lineNameArr = lineName.components(separatedBy: "-")
        lineName = lineNameArr[2]
        var stationName = chatArr[4]
        let stationNameArr = stationName.components(separatedBy: "-")
        stationName = stationNameArr[1]
        stationName = stationName.capitalizingFirstLetter()
        print("station Name: \(stationName), lineName: \(lineName)")
        
        title = "\(lineName) / \(stationName)"
        
        print(activeChat)
        
        activeChatFixed = activeChat + "/messages"
        
        //Firebase
        ref = Database.database().reference(withPath: activeChatFixed)
        fetchData()
        
        
        usersRef = Database.database().reference(withPath: "\(activeChat!)/users-online")
        
        
        

        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinatonVC = segue.destination as? GroupInfoViewController {
            
            destinatonVC.activeChat = activeChat
        }
       
        
        
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        let activeChatPush = activeChat.replacingOccurrences(of: "/", with: "_")
        Messaging.messaging().unsubscribe(fromTopic: activeChatPush)
        print("bye bye \(activeChat!)")
        
        //
        guard let user = Auth.auth().currentUser,
            let userRef = usersRef else {return}
        
        let onlineRef = userRef.child(user.uid)
        onlineRef.removeValue { (error, _) in
            if let error = error {
                print("Remove Online failed: \(error)")
                return
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let activeChatPush = activeChat.replacingOccurrences(of: "/", with: "_")
        
        print(activeChatPush)
        
        Messaging.messaging().subscribe(toTopic: activeChatPush)
        print("hello \(activeChat!)")
        
        
        //add to chat counter
        Auth.auth().addStateDidChangeListener { (auth, user) in
            guard let user = user,
                let userRef = self.usersRef else {return}
            
            let currenteUserRef = self.usersRef?.child(user.displayName!)
            currenteUserRef?.setValue(user.email)
            currenteUserRef?.onDisconnectRemoveValue()
        }
    }
    
    private func fetchData(){
        guard let ref = self.ref else { return }
        ref.observe(.value) { (snapshot) in
                //Only get the most recent message
                if let message = Message(snapshot: (snapshot.children.allObjects.last as? DataSnapshot)!) {
                    self.insertMessage(message)
                    //check if the last message is not from the sender
                    if self.messageList.last?.senderId != self.currentSender().id {
                        //play sound
                        //toca o som, lista de sons disponiveis: http://iphonedevwiki.net/index.php/AudioServices
                        AudioServicesPlayAlertSound(1007)
                        //vibra o iphone
                        AudioServicesPlayAlertSound(4095)
                    }
            }
        }
    }
    
    
    func configureMessageCollectionView() {
        
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messageCellDelegate = self
        
        scrollsToBottomOnKeyboardBeginsEditing = true // default false
        maintainPositionOnKeyboardFrameChanged = true // default false
        
        
    }
    
    func configureMessageInputBar() {
        messageInputBar.delegate = self
        
    }
    
    // MARK: - Helpers
    
    func insertMessage(_ message: Message) {
        messageList.append(message)
        // Reload last section to update header/footer labels and insert a new one
        messagesCollectionView.performBatchUpdates({
            messagesCollectionView.insertSections([messageList.count - 1])
            if messageList.count >= 2 {
                messagesCollectionView.reloadSections([messageList.count - 2])
            }
        }, completion: { [weak self] _ in
            if self?.isLastSectionVisible() == true {
                self?.messagesCollectionView.scrollToBottom(animated: true)
            }
        })
    }
    
    func isLastSectionVisible() -> Bool {
        
        guard !messageList.isEmpty else { return false }
        
        let lastIndexPath = IndexPath(item: 0, section: messageList.count - 1)
        
        return messagesCollectionView.indexPathsForVisibleItems.contains(lastIndexPath)
    }
    
    // MARK: - MessagesDataSource
    
    //Define o usuario atual
    func currentSender() -> Sender {
        guard let user = Auth.auth().currentUser,
        let name = user.displayName,
        let email = user.email
        //let email = user.email
            else {fatalError("User not logged in")}
        
        
        return Sender(id: email, displayName: name)
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messageList.count
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messageList[indexPath.section]
    }
    
    func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        if indexPath.section % 3 == 0 {
            return NSAttributedString(string: MessageKitDateFormatter.shared.string(from: message.sentDate), attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        }
        return nil
    }
    
    func messageTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let name = message.sender.displayName
        return NSAttributedString(string: name, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption1)])
    }
    
    func messageBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        
        let dateString = formatter.string(from: message.sentDate)
        return NSAttributedString(string: dateString, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption2)])
    }
    
}

// MARK: - MessageCellDelegate

extension ChatViewController: MessageCellDelegate {
    
    func didTapAvatar(in cell: MessageCollectionViewCell) {
        print("Avatar tapped")
    }
    
    func didTapMessage(in cell: MessageCollectionViewCell) {
        print("Message tapped")
    }
    
    func didTapCellTopLabel(in cell: MessageCollectionViewCell) {
        print("Top cell label tapped")
    }
    
    func didTapMessageTopLabel(in cell: MessageCollectionViewCell) {
        print("Top message label tapped")
    }
    
    func didTapMessageBottomLabel(in cell: MessageCollectionViewCell) {
        print("Bottom label tapped")
    }
    
    func didTapAccessoryView(in cell: MessageCollectionViewCell) {
        print("Accessory view tapped")
    }
    
}

// MARK: - MessageLabelDelegate

extension ChatViewController: MessageLabelDelegate {
    
    func didSelectAddress(_ addressComponents: [String: String]) {
        print("Address Selected: \(addressComponents)")
    }
    
    func didSelectDate(_ date: Date) {
        print("Date Selected: \(date)")
    }
    
    func didSelectPhoneNumber(_ phoneNumber: String) {
        print("Phone Number Selected: \(phoneNumber)")
    }
    
    func didSelectURL(_ url: URL) {
        print("URL Selected: \(url)")
    }
    
    func didSelectTransitInformation(_ transitInformation: [String: String]) {
        print("TransitInformation Selected: \(transitInformation)")
    }
    
}

// MARK: - MessageInputBarDelegate

extension ChatViewController: MessageInputBarDelegate {
    
    func messageInputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {
          
        let message = Message(text: text, sender: currentSender(), messageId: UUID().uuidString, date: Date())
        
        let messageRef = ref?.childByAutoId()
        messageRef?.setValue(message.toAnyObject())
        
        
        inputBar.inputTextView.text = String()
        messagesCollectionView.scrollToBottom(animated: true)
    }
    
   
    
}
