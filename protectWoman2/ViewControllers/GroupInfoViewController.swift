//
//  GroupInfoViewController.swift
//  protectWoman2
//
//  Created by Matheus Santos on 23/10/18.
//  Copyright © 2018 giovane barreira. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import MessageKit

class GroupInfoViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
   
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var timeLeftToLeaveLbl: UILabel!
    
    @IBOutlet weak var timeToLeave: UILabel!
    
    @IBOutlet weak var confettiView: ConffetiView!
    
    
    
    var activeChat: String!
    var closingTime: Date!
    var openingTime: Date!
    var currentTime: Date?
    private var users = [Sender]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        
        self.tableView.estimatedSectionHeaderHeight = 35
     
        self.confettiView.layer.zPosition = -5
        
        
        let messagesRef = Database.database().reference(withPath: activeChat + "/users-online")
        
        messagesRef.observe(.childAdded, with: { (snapshot) in
            guard let email = snapshot.value as? String else {return}
            self.users.append(Sender(id: snapshot.key, displayName: email))
            let lastRow = self.users.count - 1
            let indexPath = IndexPath(row: lastRow, section: 0)
            self.tableView.insertRows(at: [indexPath], with: .top)
            self.tableView.reloadSections(IndexSet(arrayLiteral: 0), with: .automatic)
            })
        
        messagesRef.observe(.childRemoved) { (snapshot) in
            guard let email =  snapshot.value as? String else {return}
            guard let index = self.users.lastIndex(of: Sender(id: snapshot.key, displayName: email)) else {return}
            
            let indexPath = IndexPath(row: index, section: 0)
            self.users.remove(at: index)
            self.tableView.deleteRows(at: [indexPath], with: .fade)
            self.tableView.reloadSections(IndexSet(arrayLiteral: 0), with: .automatic)
        }

        
        let activeChatUseful = activeChat + "/useful_stuff"
        
        let roomRef = Database.database().reference(withPath: activeChatUseful)
    
        
        roomRef.observeSingleEvent(of: .value, with: {(snapshot) in
            guard let value = snapshot.value as? [String: AnyObject] else {fatalError("could not get value")}
            guard let closingTime = value["endTime"] as? String else {fatalError("could not get closing time")}
            guard let openingTime = value["startTime"] as? String else {fatalError("could not get opening time")}
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "HH:mm"
            self.closingTime = dateFormatterGet.date(from: closingTime)
            self.openingTime = dateFormatterGet.date(from: openingTime)
            
            DispatchQueue.main.async {
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "HH:mm"
                self.timeLeftToLeaveLbl.text = "\(dateFormatterGet.string(from: self.openingTime)) -> \(dateFormatterGet.string(from: self.closingTime))"
            }
        })
        
        NetworkTime().serverTimeReturn { (getResDate) -> Void in
            let dFormatter = DateFormatter()
            dFormatter.dateFormat = "HH:mm:ss"
            let dateGet = dFormatter.string(from: getResDate!)
            DispatchQueue.main.async {
                self.timeToLeave.text = dateGet
                self.currentTime = getResDate!
            }
            
        }
        
       let timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: {_ in
        if self.currentTime != nil {
                self.currentTime!.addTimeInterval(1)
                let dFormatter = DateFormatter()
                dFormatter.dateFormat = "HH:mm:ss"
                let dateGet = dFormatter.string(from: self.currentTime!)
                DispatchQueue.main.async {
                    self.timeToLeave.text = dateGet
                }
        }
        })
    }
    

   
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "\(users.count) participantes"
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "groupUserInfo")
        cell.textLabel?.text = users[indexPath.row].id
        cell.detailTextLabel?.text = users[indexPath.row].displayName
        cell.accessoryType = .detailButton
        
        return cell
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
