//
//  ChatsTableViewController.swift
//  ProtectWoman
//
//  Created by Matheus Santos on 15/10/18.
//  Copyright © 2018 Matheus Santos. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class ChatsTableViewControllers: UITableViewController {
    
    private var chats: [String] = []
    private var chatsRef: DatabaseReference?
    
    private var clickedChat: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Auth.auth().currentUser == nil {
           self.performSegue(withIdentifier: "toLogin", sender: self)
        }
        
        
        chatsRef = Database.database().reference(withPath: "rooms")
        guard let chatsRef = chatsRef else {return}
        
       
        chatsRef.observeSingleEvent(of: .value, with: { (snapshot) in
            for child in snapshot.children {
                if let rooms = child as? DataSnapshot {
                    self.chats.append(rooms.key)
                    
                }
            }
            self.tableView.reloadData()
            
            })
        
        chatsRef.observe(.childAdded, with: {(snapshot) in
            guard let room = snapshot.value as? String else {return}
            self.chats.append(room)
            let lastRow = self.chats.count - 1
            let indexPath = IndexPath(row: lastRow, section: 0)
            self.tableView.insertRows(at: [indexPath], with: .top)
        })
        
        chatsRef.observe(.childRemoved) { (snapshot) in
            guard let room = snapshot.value as? String else {return}
            guard let index = self.chats.lastIndex(of: room) else {return}
            
            let indexPath = IndexPath(row: index, section: 0)
            self.chats.remove(at: index)
            self.tableView.deleteRows(at: [indexPath], with: .fade)
        }
        
        
        
    }
    
    // MARK: - Table view data source
    
    @IBAction func logoutBtn(_ sender: Any) {
        guard let user = Auth.auth().currentUser else {return}
        
        do{
            try Auth.auth().signOut()
            self.performSegue(withIdentifier: "toLogin", sender: self)

        }
        catch(let error){
            print("Auth sign out failed: \(error)")
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return chats.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: .default, reuseIdentifier: "roomCell")
        let timeToLeave = self.chats[indexPath.row]
        
        cell.textLabel?.text = timeToLeave
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(chats[indexPath.row])
        
        clickedChat = chats[indexPath.row]
        
        performSegue(withIdentifier: "toChat", sender: self)
        
        
    }
    
    //MARK: Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    
}

