//
//  LinesCollectionViewCell.swift
//  protectWoman2
//
//  Created by Bruna Gagliardi on 23/10/18.
//  Copyright © 2018 giovane barreira. All rights reserved.
//

import UIKit

class LinesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var linesLabel: UILabel!
    @IBOutlet weak var stationLabel: UILabel!
    @IBOutlet weak var simbolLabel: UILabel!
}
