//
//  StationCollectionViewCell.swift
//  protectWoman2
//
//  Created by Bruna Gagliardi on 24/10/18.
//  Copyright © 2018 giovane barreira. All rights reserved.
//

import UIKit

class StationCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var stationLabel: UILabel!
    
    @IBOutlet weak var numberUsersLabel: UILabel!
    
}
