//
//  ConffetiView.swift
//  protectWoman2
//
//  Created by Matheus Santos on 23/10/18.
//  Copyright © 2018 giovane barreira. All rights reserved.
//

import UIKit

class ConffetiView: UIView {

    var emitter = CAEmitterLayer()
    
    var colors:[UIColor] = [
        Colors.red,
        Colors.blue,
        Colors.green,
        Colors.yellow
    ]
    
    var images:[UIImage] = [
        Images.box,
        Images.triangle,
        Images.circle,
        Images.box
    ]
    
    var velocities:[Int] = [
        70,
        90,
        30,
        100
    ]

   override func didMoveToWindow() {
        //
        emitter.emitterPosition = CGPoint(x: self.frame.size.width / 2, y: 0)
        emitter.emitterShape = CAEmitterLayerEmitterShape.line
        emitter.emitterSize = CGSize(width: self.frame.size.width, height: 2.0)
        emitter.emitterCells = generateEmitterCells()
        emitter.zPosition = -5
        self.layer.addSublayer(emitter)
        
    
    }
    
    
    private func generateEmitterCells() -> [CAEmitterCell] {
        var cells:[CAEmitterCell] = [CAEmitterCell]()
        for index in 0..<16 {
            
            let cell = CAEmitterCell()
            
            cell.birthRate = 4.0
            cell.lifetime = 14.0
            cell.lifetimeRange = 0
            cell.velocity = CGFloat(getRandomVelocity())
            cell.velocityRange = 0
            cell.emissionLongitude = CGFloat(Double.pi)
            cell.emissionRange = 0.5
            cell.spin = 3.5
            cell.spinRange = 0
            cell.color = getNextColor(i: index)
            cell.contents = getNextImage(i: index)
            cell.scaleRange = 0.25
            cell.scale = 0.1
            
            cells.append(cell)
            
        }
        
        return cells
        
    }
    
    private func getRandomVelocity() -> Int {
        return velocities[getRandomNumber()]
    }
    
    private func getRandomNumber() -> Int {
        return Int(arc4random_uniform(4))
    }
    
    private func getNextColor(i:Int) -> CGColor {
        if i <= 4 {
            return colors[0].cgColor
        } else if i <= 8 {
            return colors[1].cgColor
        } else if i <= 12 {
            return colors[2].cgColor
        } else {
            return colors[3].cgColor
        }
    }
    
    private func getNextImage(i:Int) -> CGImage {
        return images[i % 4].cgImage!
    }

}


enum Colors {
    
    static let red = #colorLiteral(red: 0.9176128507, green: 0.4962953329, blue: 0.4400758445, alpha: 1)
    static let blue = #colorLiteral(red: 0.6119356751, green: 0.4198369086, blue: 0.3420306444, alpha: 1)
    static let green = #colorLiteral(red: 1, green: 0.7535283566, blue: 0.641972363, alpha: 1)
    static let yellow = #colorLiteral(red: 0.6832941771, green: 0.4514272213, blue: 0.3389413655, alpha: 1)
    
}

enum Images {
    
    static let box = UIImage(named: "Box")!
    static let triangle = UIImage(named: "Triangle")!
    static let circle = UIImage(named: "Circle")!
    static let swirl = UIImage(named: "Spiral")!
    
}

