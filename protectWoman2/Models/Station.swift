//
//  Station.swift
//  protectWoman2
//
//  Created by Bruna Gagliardi on 22/10/18.
//  Copyright © 2018 giovane barreira. All rights reserved.
//

import Foundation
import FirebaseDatabase

class Lines {
    var key : String!
    var name : String!
    var stationArray = [Station]()
    var firstStation: String!
    var lastStation: String!
    
    init(snapshot: DataSnapshot) {
        guard let dict = snapshot.value as? [String: Any],
        let titleStation = dict["title-station"] as? String
        else {return}
        let stationsSnap = snapshot.childSnapshot(forPath: "Stations")
        guard let dictFirst = snapshot.value as? [String: Any],
            let firstSnap = dictFirst["First"] as? String
            else {return}
        
        guard let dictLast = snapshot.value as? [String: Any],
            let lastSnap = dictLast["Last"] as? String
            else {return}

        self.key = snapshot.key
        self.name = titleStation
        self.firstStation = firstSnap
        self.lastStation = lastSnap
        
        for station in stationsSnap.children {
            guard let stationSnap = station as? DataSnapshot else {return}
            let stationObj = Station(snapshot: stationSnap)
            self.stationArray.append(stationObj)
        }
    }
}
class Station {
    var key: String!
    var name: String!
    
    init(snapshot: DataSnapshot) {
        guard let dict = snapshot.value as? [String: Any],
            let stationName = dict["title"] as? String
            else {return}
        self.key = snapshot.key
        self.name = stationName
    }
}
