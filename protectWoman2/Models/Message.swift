//
//  Message.swift
//  ProtectWoman
//
//  Created by Matheus Santos on 15/10/18.
//  Copyright © 2018 Matheus Santos. All rights reserved.
//
import UIKit
import MessageKit
import FirebaseDatabase

internal struct Message: MessageType {

    var sender: Sender
    var sentDate: Date
    var kind: MessageKind

    //vars in strings
    var senderId: String!
    var sentDateString: String!
    var messageText: String!
    var messageId: String

    //FireBaseFunctions
    
//    init(kind: MessageKind, sender: Sender, messageId: String, date: Date) {
//        self.kind = kind
//        self.sender = sender
//        self.messageId = messageId
//        self.sentDate = date
//    }

    init(text: String, sender: Sender, messageId: String, date: Date) {
        self.messageText = text
        self.sender = sender
        self.senderId = sender.id
        self.messageId = messageId
        self.sentDate = date
        self.kind = .text(text)
    }

    init?(snapshot: DataSnapshot) {
        guard
            let value = snapshot.value as? [String: AnyObject],
            let senderID = value["senderId"] as? String,
            let senderName = value["senderName"] as? String,
            let messageText = value["messageText"] as? String,
            let date = value["date"] as? String,
            let messageId = value["messageId"] as? String
            else {return nil}
        //get date from string
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm"
        
        self.init(text: messageText, sender: Sender(id: senderID, displayName: senderName), messageId: messageId, date: dateFormatterGet.date(from: date) ?? Date.distantPast)
        
    }
    
    func toAnyObject() -> Any {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        let dateString = dateFormatter.string(from: sentDate)
        
        return [
            "senderId": sender.id,
            "senderName": sender.displayName,
            "messageText": messageText,
            "date": dateString,
            "messageId": messageId
        ]
        
    }
}
