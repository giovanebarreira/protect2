//
//  NetworkTime.swift
//  protectWoman2
//
//  Created by Matheus Santos on 24/10/18.
//  Copyright © 2018 giovane barreira. All rights reserved.
//

import Foundation

struct NetworkTime {
    func serverTimeReturn(completionHandler:@escaping (_ getResDate: Date?) -> Void) {
        let url = NSURL(string: "http://www.google.com")
        let task = URLSession.shared.dataTask(with: url! as URL) {(data, response, error) in
            let httpResponse = response as? HTTPURLResponse
            if let contentType = httpResponse!.allHeaderFields["Date"] as? String {
                let dFormatter = DateFormatter()
                dFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss z"
                let serverTime = dFormatter.date(from: contentType)
                completionHandler(serverTime)
            }
        }
        task.resume()
    }
}
