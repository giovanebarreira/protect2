//import firebase functions modules
const functions = require('firebase-functions');
//import admin module

//initialize the app
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);


// // Listens for new messages added to messages/:pushId
// exports.pushNotification = functions.database.ref('/').onWrite( event => {

//     console.log('Push notification event triggered');

//     //  Grab the current value of what was written to the Realtime Database.
    

//   // Create a notification
//     const payload = {
//         notification: {
//             title: "ProtectWoman",
//             body: "Olá você tem uma nova mensagem!",
//             sound: "default"
//         },
//     };

//   //Create an options object that contains the time to live for the notification and the priority
//     const options = {
//         priority: "high",
//         timeToLive: 60 * 60 * 24
//     };


//     return admin.messaging().sendToTopic("pushNotifications", payload, options);
// });


exports.sendPushNotification = functions.database.ref("/").onWrite(event => { //onde tem a barra, escrever ('nome da sala/{id}')

    const payload = {
        notification:{
            title: 'Nova mensagem',
            body: 'le ai logo!',
            badge: '1',
            sound: 'default',
        }  
    };
    return admin.database().ref('fcmToken').once('value').then(allToken => {
        if(allToken.val()){
            const token = Object.keys(allToken.val());
            return admin.messaging().sendToDevice(token, payload).then(response => {

            });
            
        };
    });
});

